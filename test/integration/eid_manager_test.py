from __future__ import division, absolute_import, print_function, \
    unicode_literals

import pytest

from mosaik_eid.eid.eid_manager import EidManager
from mosaik_eid.eid.entity_type_registry import EntityTypeRegistry
from mosaik_eid.eid.entity_exception import EntityTypeException
from mosaik_eid.eid.entity_type import EntityType

# succeeding cases


def test_eid_manager_from_string():
    _ = EidManager(e_type='Model')


def test_eid_manager_from_entity_type():
    _ = EidManager(e_type=EntityType.Model)


def test_eid_manager_from_entity_registry():
    _ = EidManager(e_type=EntityTypeRegistry.types.Model)


# -----------------------------------------------------


def test_eid_manager_next_eid_once():
    eid = next(EidManager(e_type=EntityType.Model))
    assert eid == 'Model-0'


def test_eid_manager_next_eid_twice():
    eid = next(EidManager(e_type=EntityType.Model))
    assert eid == 'Model-1'


def test_eid_manager_next_eid_threefold():
    eid = next(EidManager(e_type=EntityType.Model))
    assert eid == 'Model-2'


def test_eid_manager_next_eid_string():
    manager = EidManager(e_type='Model')
    eid = next(manager)
    assert eid == 'Model-3'


# tolerated cases


def test_eid_manager_register_lower_case():
    e_type = EntityTypeRegistry.register(e_type='biogas')
    eid = next(EidManager(e_type=e_type))
    assert eid == 'biogas-0'


def test_eid_manager_register_cap_words():
    e_type = EntityTypeRegistry.register(e_type='BioGas')
    eid = next(EidManager(e_type=e_type))
    assert eid == 'BioGas-0'


def test_eid_manager_register_camel_case():
    e_type = EntityTypeRegistry.register(e_type='bioGas')
    eid = next(EidManager(e_type=e_type))
    assert eid == 'bioGas-0'


def test_eid_manager_register_all_caps():
    e_type = EntityTypeRegistry.register(e_type='CHP')
    eid = next(EidManager(e_type=e_type))
    assert eid == 'CHP-0'


def test_eid_manager_register_underscores():
    e_type = EntityTypeRegistry.register(e_type='lv_num_bat_liion_11_3_30')
    eid = next(EidManager(e_type=e_type))
    assert eid == 'lv_num_bat_liion_11_3_30-0'


def test_eid_manager_register_leading_underscore():
    e_type = EntityTypeRegistry.register(e_type='_biogas')
    eid = next(EidManager(e_type=e_type))
    assert eid == '_biogas-0'


def test_eid_manager_register_tailing_underscore():
    e_type = EntityTypeRegistry.register(e_type='biogas_')
    eid = next(EidManager(e_type=e_type))
    assert eid == 'biogas_-0'


def test_eid_manager_register_tailing_number():
    e_type = EntityTypeRegistry.register(e_type='biogas42')
    eid = next(EidManager(e_type=e_type))
    assert eid == 'biogas42-0'


# failing cases


def test_eid_manager_generate_unregistered_entity_type():
    with pytest.raises(EntityTypeException) as _:
        _ = next(EidManager(e_type='unregistered'))


def test_eid_manager_register_entity_type_twice():
    with pytest.raises(EntityTypeException) as _:
        _ = EntityTypeRegistry.register(e_type='Agent')


def test_eid_manager_unregister_unregistered_entity_type():
    with pytest.raises(EntityTypeException) as _:
        EntityTypeRegistry.unregister(e_type='DoesNotExist')


def test_eid_manager_unregister_issued_entity_type_string():
    e_type = EntityTypeRegistry.register('IssuedTypeString')
    _ = next(EidManager(e_type=e_type))
    with pytest.raises(EntityTypeException) as _:
        EntityTypeRegistry.unregister(e_type='IssuedTypeString')


def test_eid_manager_unregister_issued_entity_type_object():
    e_type = EntityTypeRegistry.register('IssuedTypeObject')
    _ = next(EidManager(e_type=e_type))
    with pytest.raises(EntityTypeException) as _:
        _ = EntityTypeRegistry.unregister(e_type=e_type)


def test_eid_manager_iterable():
    manager = EidManager(EntityTypeRegistry.types.Agent)
    _ = iter(manager)


def test_eid_manager_iterator():
    manager = EidManager(EntityTypeRegistry.types.Agent)
    for (index, eid) in zip(range(3), manager):
        assert str(index) == eid[-1]


def test_eid_manager_python2_compatible_next():
    _ = EidManager(e_type=EntityType.Model).next()


def test_eid_manager_register_leading_number():
    with pytest.raises(EntityTypeException) as _:
        _ = EntityTypeRegistry.register(e_type='42biogas')
        # Accessing the entity type will not work:
        # print(EidRegistry.types.42biogas)
