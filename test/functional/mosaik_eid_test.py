from __future__ import division, absolute_import, print_function, \
    unicode_literals

from mosaik_eid.eid.eid_manager import EidManager
from mosaik_eid.eid.entity_type_registry import EntityTypeRegistry

# succeeding cases


def test_mosaik_eid_register_unregister_cap_words():
    _ = EntityTypeRegistry.register(e_type='MyType')
    _ = EntityTypeRegistry.unregister(e_type='MyType')
    _ = EntityTypeRegistry.register(e_type='MyType')


def test_mosaik_eid_register_unregister_camel_case():
    _ = EntityTypeRegistry.register(e_type='myType')
    _ = EntityTypeRegistry.unregister(e_type='myType')
    _ = EntityTypeRegistry.register(e_type='myType')


def test_mosaik_eid_register_unregister_all_caps():
    _ = EntityTypeRegistry.register(e_type='MY_TYPE')
    _ = EntityTypeRegistry.unregister(e_type='MY_TYPE')
    _ = EntityTypeRegistry.register(e_type='MY_TYPE')


def test_mosaik_eid_register_issue():
    e_type = EntityTypeRegistry.register(e_type='lithium_polymer')
    eid = next(EidManager(e_type=e_type))
    assert eid == 'lithium_polymer-0'


def test_mosaik_eid_register_issue_repeatedly():
    e_type = EntityTypeRegistry.register(e_type='wind')

    eid = next(EidManager(e_type=e_type))
    assert eid == 'wind-0'

    eid = next(EidManager(e_type=e_type))
    assert eid == 'wind-1'


def test_mosaik_eid_register_issue_iteratively():
    e_type = EntityTypeRegistry.register(e_type='redox')

    for index in range(3):
        eid = next(EidManager(e_type=e_type))

        assert eid[-1] == str(index)
