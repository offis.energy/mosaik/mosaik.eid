from __future__ import division, absolute_import, print_function, \
    unicode_literals

import pytest

from mosaik_eid.eid.entity_identifier import EntityIdentifier
from mosaik_eid.eid.entity_exception import EntityIndexException, \
    EntityTypeException
from mosaik_eid.eid.entity_type import EntityType


# succeeding cases


def test_eid_generator_generate_agent():
    eid = EntityIdentifier(EntityType.Agent, str(1))
    assert eid == "Agent-1"


def test_eid_generator_generate_controller():
    eid = EntityIdentifier(EntityType.Controller, str(1))
    assert eid == "Controller-1"


def test_eid_generator_generate_model():
    eid = EntityIdentifier(EntityType.Model, str(1))
    assert eid == "Model-1"


def test_eid_generator_generate_monitor():
    eid = EntityIdentifier(EntityType.Monitor, str(1))
    assert eid == "Monitor-1"


# tolerated cases


def test_eid_generator_generate_big_index():
    eid = EntityIdentifier(EntityType.Model, str(10 ** 10))
    assert eid == "Model-" + str(10 ** 10)


def test_eid_generator_generate_zero_index():
    eid = EntityIdentifier(EntityType.Model, str(0))
    assert eid == "Model-0"


def test_eid_generator_generate_int_index():
    eid = EntityIdentifier(EntityType.Model, 10 ** 10)
    assert eid == "Model-" + str(10 ** 10)


# failing cases


def test_eid_generator_generate_negative_index_raises():
    with pytest.raises(EntityIndexException) as _:
        _ = EntityIdentifier(EntityType.Model, -1)


def test_eid_generator_generate_non_entity_type_raises():
    with pytest.raises(EntityTypeException) as _:
        _ = EntityIdentifier("tomato", 0)


def test_eid_generator_generate_non_numeric_index_raises():
    with pytest.raises(EntityIndexException) as _:
        _ = EntityIdentifier(EntityType.Model, "a")
