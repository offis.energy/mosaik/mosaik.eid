from __future__ import division, absolute_import, print_function, \
    unicode_literals

from enum import EnumMeta

from mosaik_eid.eid.entity_type import EntityType


# succeeding cases


def test_eid_type_type():
    result = type(EntityType)
    assert result == EnumMeta


def test_eid_type_agent_type():
    assert str(EntityType.Agent) == 'EntityType.Agent'


def test_eid_type_controller_type():
    assert str(EntityType.Controller) == 'EntityType.Controller'


def test_eid_type_model_type():
    assert str(EntityType.Model) == 'EntityType.Model'


def test_eid_type_monitor_type():
    assert str(EntityType.Monitor) == 'EntityType.Monitor'
