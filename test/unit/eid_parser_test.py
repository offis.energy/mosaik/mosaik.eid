from __future__ import division, absolute_import, print_function, \
    unicode_literals

import pytest

from mosaik_eid.eid.eid_parser import EidParser
from mosaik_eid.eid.entity_exception import EntityIndexException, \
    EntityTypeException, EntityException
from mosaik_eid.eid.entity_type import EntityType


# succeeding cases

def test_eid_parser_parse_agent():
    eid = "Agent-1"

    eid_type, index = EidParser.parse(eid)

    assert eid_type.name == EntityType.Agent.name
    assert index == 1


def test_eid_parser_parse_model():
    eid = "Model-1"

    eid_type, index = EidParser.parse(eid)

    assert eid_type.name == EntityType.Model.name
    assert index == 1


def test_eid_parser_parse_monitor():
    eid = "Monitor-1"

    eid_type, index = EidParser.parse(eid)

    assert eid_type.name == EntityType.Monitor.name
    assert index == 1


def test_eid_parser_parse_controller():
    eid = "Controller-1"

    eid_type, index = EidParser.parse(eid)

    assert eid_type.name == EntityType.Controller.name
    assert index == 1


# tolerated cases


def test_eid_parser_parse_big_index():
    eid = "Model-12345678901234567890123457890"

    eid_type, index = EidParser.parse(eid)

    assert eid_type.name == EntityType.Model.name
    assert index == 12345678901234567890123457890


def test_eid_parser_parse_zero_index():
    eid = "Model-0"

    eid_type, index = EidParser.parse(eid)

    assert eid_type.name == EntityType.Model.name
    assert index == 0


# failing cases


def test_eid_parser_parse_negative_index_raises():
    eid = "Model--1"

    with pytest.raises(EntityException) as _:
        _, _ = EidParser.parse(eid)


def test_eid_parser_parse_non_numeric_index_raises():
    eid = "Model-a"

    with pytest.raises(EntityIndexException) as _:
        _, _ = EidParser.parse(eid)


def test_eid_parser_parse_non_entity_type_raises():
    eid = "Tomato-1"

    with pytest.raises(EntityTypeException) as _:
        _, _ = EidParser.parse(eid)
