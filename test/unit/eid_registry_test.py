from __future__ import division, absolute_import, print_function, \
    unicode_literals

import pytest

from mosaik_eid.eid.entity_type_registry import EntityTypeRegistry
from mosaik_eid.eid.entity_exception import EntityTypeException


# succeeding cases

def test_eid_registry_register_string():
    e_type = EntityTypeRegistry.register(e_type='EIDR_register_string')
    assert EntityTypeRegistry.registered(e_type=e_type)


def test_eid_registry_register_another_string():
    e_type = EntityTypeRegistry.register(e_type='EIDR_register_another_string')
    assert EntityTypeRegistry.registered(e_type=e_type)


def test_eid_registry_unregister_registered_string():
    e_type = EntityTypeRegistry.register('EIDR_unregister_registered_string')
    EntityTypeRegistry.unregister(e_type=e_type)
    assert not EntityTypeRegistry.registered(e_type=e_type)


def test_eid_registry_unregister_registered_object():
    e_type = EntityTypeRegistry.register(
        e_type='EIDR_unregister_registered_object')
    EntityTypeRegistry.unregister(e_type=e_type)
    assert not EntityTypeRegistry.registered(e_type=e_type)


def test_eid_registry_unregister_unused_string():
    e_type = EntityTypeRegistry.register(e_type='EIDR_unregister_unused')
    EntityTypeRegistry.unregister(e_type=e_type)

# tolerated cases


def test_eid_registry_register_lower_case():
    e_type = EntityTypeRegistry.register(e_type='coldstorage')
    result = EntityTypeRegistry.registered(e_type=e_type)
    assert result


def test_eid_registry_register_cap_words():
    e_type = EntityTypeRegistry.register(e_type='ColdStorage')
    result = EntityTypeRegistry.registered(e_type=e_type)
    assert result


def test_eid_registry_register_camel_case():
    e_type = EntityTypeRegistry.register(e_type='coldStorage')
    result = EntityTypeRegistry.registered(e_type=e_type)
    assert result


def test_eid_registry_register_all_caps():
    e_type = EntityTypeRegistry.register(e_type='CS')
    result = EntityTypeRegistry.registered(e_type=e_type)
    assert result


def test_eid_registry_register_underscores():
    e_type = EntityTypeRegistry.register(e_type='cold_storage')
    result = EntityTypeRegistry.registered(e_type=e_type)
    assert result


# failing cases


def test_eid_registry_register_string_twice():
    e_type = EntityTypeRegistry.register(e_type='EIDR_register_string_twice')
    with pytest.raises(EntityTypeException) as _:
        EntityTypeRegistry.register(e_type=e_type)


def test_eid_registry_unregister_unregistered_string():
    with pytest.raises(EntityTypeException) as _:
        EntityTypeRegistry.unregister(e_type='EIDR_unregistered')
