Done

Implement EidManager.__iter__(self)
Make EidManager an Iterator object
Implement EidManager.unregister(self, e_type)
Split EidManager into EidManagerIterator, EidManagerClass and EidManager

WontFix

Make Eidmanager.register a context manager object (does not make sense)
Hint types in parser and generator (requires >= Python 3.5)
Allow '-' in entity type name (invalid Python syntax)

ToDo

Use common information model (CIM)
Make EidManager thread-safe
Implement a parameter list: CHP-1_p:12_c:40
